import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import javax.swing.Timer;

public class Model {
	private int width;
	private int height;
	private boolean[][] actualField;
	private boolean[][] nextField;
	private boolean alive;
	private boolean nextround;
	private List<boolean[][]> fieldHistory = new LinkedList<boolean[][]>();
	private int saves;
	private boolean play;
	private int generation;
	private float aliveChance;
	private int refreshRate;
	public Timer updateTimer;
	
	private int currentHistoryIndex;
	private int stepBackInTime;
	private int newHistoryIndex;
	private boolean goBack;
	
	private final boolean dubug = false;
	
	private int updateCounter = 0;
	private int indexCounter = 0;
	

	public Model() {
		this.init();
	}
	
	public Model(int width, int height, float aliveChance, int refreshRate, int saves) {
		this.width = width;
		this.height = height;
		this.actualField = new boolean[width][height];
		this.nextField = new boolean[width][height];
		this.aliveChance = aliveChance;
		this.refreshRate = refreshRate;
		this.updateTimer = new Timer(refreshRate, new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				nextGeneration();
			}
		});
		this.saves = saves;
		this.init();
		
		this.currentHistoryIndex = 0;
		this.stepBackInTime = 0;
		this.newHistoryIndex = 0;
		this.goBack = false;
		this.play = false;
		this.generation = 0;
	}

	public int getSaves() {
		return this.saves;
	}
	
	public int getFieldHistorySize() {
		return this.fieldHistory.size();
	}
	
	public int getWidth() {
		return this.width;
	}

	public boolean getPlayStatus() {
		return this.play;
	}

	public int getHeight() {
		return this.height;
	}

	public void changeState(int x, int y) {
		if (actualField[x][y])
			actualField[x][y] = false;
		else
			actualField[x][y] = true;
	}

	public void clear() {
		for (int x = 0; x < this.width; x++) {
			for (int y = 0; y < this.height; y++) {
				this.actualField[x][y] = false;
			}
		}
		this.fieldHistory.clear();
		this.currentHistoryIndex = 0;
		this.stepBackInTime = 0;
		this.newHistoryIndex = 0;
		this.indexCounter = 0;
		this.generation = 0;

	}

	public void init() {
		for (int x = 0; x < this.width; x++) {
			for (int y = 0; y < this.height; y++) {
				this.actualField[x][y] = setState(this.aliveChance);
			}
		}
		this.fieldHistory.clear();
		this.currentHistoryIndex = 0;
		this.stepBackInTime = 0;
		this.newHistoryIndex = 0;
		this.indexCounter = 0;
		this.generation = 0;

	}

	public boolean isAlive(int x, int y) {
		
		return this.actualField[x][y];
	}
	
	public int getGeneration(){
		return this.generation;
	}

	public void nextGeneration() {
		this.generation++;
		long debugTimer1 = 0;
		long debugTimer2 = 0;
		if (this.getPlayStatus()) {
			if(this.dubug){ debugTimer1 = System.currentTimeMillis();}
			conwayRules();
			updateCounter++;
			if(this.dubug){ debugTimer2 = System.currentTimeMillis();}
			if(this.dubug){ System.out.println(updateCounter + " Update takes ["+ (debugTimer2 - debugTimer1) + "] ms.");}
			for (int x = 0; x < this.width; x++) {
				for (int y = 0; y < this.height; y++) {
					this.actualField[x][y] = this.nextField[x][y];
				}
			}
			
			this.addToHistory(this.actualField);
		}

	}
	
	private void addToHistory(boolean[][] s){
		
		if(this.fieldHistory.isEmpty())
		{
			this.fieldHistory.add(this.cloneBoolField(s, this.width, this.height));
			this.currentHistoryIndex = this.modulo(this.indexCounter, this.saves);
		}else{
			if(this.fieldHistory.size() < this.saves){
				this.currentHistoryIndex = this.modulo(this.indexCounter, this.saves);
				this.fieldHistory.add(this.currentHistoryIndex, this.cloneBoolField(s, this.width, this.height));
			}
			else{
				this.currentHistoryIndex = this.modulo(this.indexCounter, this.saves);
				this.fieldHistory.remove(currentHistoryIndex);
				this.fieldHistory.add(this.currentHistoryIndex, this.cloneBoolField(s, this.width, this.height));
			}
		}
		
		this.indexCounter++;
	}

	public void previousGeneration() {

	}
	
	private boolean[][] cloneBoolField(boolean[][] b, int w, int h){
		boolean[][] newField = new boolean[w][h];
		for(int i=0; i<w; i++){
			for(int j=0; j<h; j++){
				newField[i][j] = b[i][j];
			}
		}
		return newField;
	}

	private void conwayRules() {
		for (int x = 0; x < this.width; x++) {
			for (int y = 0; y < this.height; y++) {
				int mx = x - 1;
				if (mx < 0)
					mx = width - 1;
				int my = y - 1;
				if (my < 0)
					my = height - 1;
				int gx = (x + 1) % width;
				int gy = (y + 1) % height;

				int alivecounter = 0;

				if (isAlive(mx, my))
					alivecounter++;
				if (isAlive(mx, y))
					alivecounter++;
				if (isAlive(mx, gy))
					alivecounter++;
				if (isAlive(x, my))
					alivecounter++;
				if (isAlive(x, gy))
					alivecounter++;
				if (isAlive(gx, my))
					alivecounter++;
				if (isAlive(gx, y))
					alivecounter++;
				if (isAlive(gx, gy))
					alivecounter++;

				if (alivecounter < 2 || alivecounter > 3) {
					nextField[x][y] = false;
				}
				else {
					if(actualField[x][y]) {
						nextField[x][y] = true;
					}else {
						nextField[x][y] = false;
					}
				}
				if (alivecounter == 3) {
					nextField[x][y] = true;
				}
			}
		}
	}

	private boolean setState(float chance) {
		Random rand = new Random();
		if (rand.nextFloat() < (chance/100.0)) {
			return true;
		} else
			return false;
	}

	public boolean[][] getActualField() {
		return this.actualField;
	}

	public void setPlay() {
		updateTimer.start();
		this.play = true;
		this.goBack = true;

	}
	
	private int modulo(int i, int j){
		return (( i % j) + j) % j;
	}

	public void stepBack() {
		this.play = false;
				
		if(this.fieldHistory.isEmpty()){
			this.goBack = false;
		}else if((this.fieldHistory.size() < this.saves) && this.goBack){
			this.stepBackInTime++;
			this.newHistoryIndex = this.currentHistoryIndex - this.stepBackInTime;
			this.generation--;
			if(this.newHistoryIndex<0){
				this.goBack = false;
			}else{
				this.actualField = this.fieldHistory.get(this.newHistoryIndex);
			}
		} else if(this.goBack){
			this.stepBackInTime++;
			this.newHistoryIndex = this.modulo((this.currentHistoryIndex - this.stepBackInTime), this.fieldHistory.size());
			
			if(this.newHistoryIndex != this.currentHistoryIndex){
				this.generation--;
				this.actualField = this.fieldHistory.get(this.newHistoryIndex);
			}else {
				this.newHistoryIndex = this.modulo(this.currentHistoryIndex + 1, this.fieldHistory.size());
				this.goBack = false;
			}
			
		}
	}

	
	public void setPause() {
		updateTimer.stop();
		this.play = false;
	}
	
	private boolean compareBoolFields(boolean[][] a, boolean[][] b){
		int tempBool = 0;
		for(int i = 0; i< this.width; i++){
			for(int j = 0; j< this.width; j++){
				if(a[i][j] == b[i][j]) tempBool++;
			}
		}
		if(tempBool == (this.width*this.height)) return true;
		else return false;
	}
	
	private void debugBoolField(boolean[][] field){
		int temp = 0;
		for(int i=0; i<this.width; i++){
			for(int j=0; j<this.height; j++){
				if((field[i][j])){ System.out.println("[" + i + "," + j + "]"); temp++;}
				
			}
		}
		System.out.println(temp);
		System.out.println("--Ende--");
	}
	
	private void testField(List<boolean[][]> b){
		for(int i = 0; i<b.size(); i++){
			if((i+1) < b.size()) System.out.println(compareBoolFields(b.get(i), b.get(i+1)));
		}
	}
	
	public void setAlive(int x, int y){
		if(this.actualField[x][y] == true) this.actualField[x][y] = false;
		else this.actualField[x][y] = true;
		this.addToHistory(this.actualField);
		this.goBack = true;
		this.generation++;
	}
	
	public void setAlive(int x, int y, boolean b){
		this.actualField[x][y] = b;
		this.addToHistory(this.actualField);
		this.goBack = true;
		this.generation++;
	}
}
