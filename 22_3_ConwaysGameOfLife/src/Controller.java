import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import javax.swing.JButton;
import javax.swing.Timer;

public class Controller implements ActionListener, MouseListener, MouseMotionListener {

	//private Timer timer;
	private View view;
	private Model model;
	private int button;

	public Controller(Model m, View v) {
		this.model = m;
		this.view = v;
	}

	public void pause() {

	}

	public void clear() {

	}

	public void random() {

	}

	public void clickedField(int row, int col) {

	}

	
	// ("play")); // 0
	// ("pause"));// 1
	// ("back")); // 2
	// ("clear")); //3
	// ("random")); //4
	// ("exit")); //5
	
//	Timer timer = new Timer(250, new ActionListener() {
//		public void actionPerformed(ActionEvent ev) {
//		System.out.println("250 Milisekunden sind um.");
//		
//		}
//		});
	
	
	
	
	public void actionPerformed(ActionEvent ev) {
		if (ev.getSource() == view.buttons.get(0)) { // Play
			model.setPlay();
			view.buttons.get(0).setEnabled(false); // Wenn Play gedrückt wird setze Play auf disabled und Pause auf enabled
			view.buttons.get(1).setEnabled(true);
			view.buttons.get(2).setEnabled(false);
			view.buttons.get(3).setEnabled(false);
			view.buttons.get(4).setEnabled(false);
			

		}
		if (ev.getSource() == view.buttons.get(1)) { // Pause
			model.setPause();
			view.buttons.get(0).setEnabled(true);
			view.buttons.get(1).setEnabled(false); // Wenn Pause gedrückt wird setze Play auf enabled und Pause auf disabled
			view.buttons.get(2).setEnabled(true);
			view.buttons.get(3).setEnabled(true);
			view.buttons.get(4).setEnabled(true);
		}
		if (ev.getSource() == view.buttons.get(2)) { // Back
			model.stepBack();
			view.buttons.get(0).setEnabled(true);
			view.buttons.get(1).setEnabled(false);
			view.update();
			view.repaint();
		}
			
		if (ev.getSource() == view.buttons.get(3)) { // Clear
			model.clear();
		}
			
		if (ev.getSource() == view.buttons.get(4)) { // Reset
			model.init();
		}
		
		if(ev.getSource() == view.buttons.get(5)) { // EXIT
			System.exit(0);
		}
			
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		this.button = e.getButton();
		if(this.view.getPlayStatus() != true){
			int mx = ((JGoLField)e.getSource()).getRow();
			int my = ((JGoLField)e.getSource()).getCol();
			System.out.println(mx + "," + my);
			this.model.setAlive(mx, my);
		}
		
		
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		button = -1;
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseDragged(MouseEvent e) {
//		if(this.view.getPlayStatus() != true)
//		{
//			int mx = ((JGoLField)e.getSource()).getRow();
//			int my = ((JGoLField)e.getSource()).getCol();
//			if(button == 1) this.model.setAlive(mx, my, true);
//			else this.model.setAlive(mx, my, false);;
//		}
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

}
