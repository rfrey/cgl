import javax.swing.plaf.basic.BasicInternalFrameTitlePane.MaximizeAction;

/**
 * Main Class for CGL
 */
public class Main {

	private static float PAUSETIME = 250.0f;
	private static float tslu;
	private static View gol;
	public static int width;
	public static int height;

	public static void main(String[] args) {

		gol = new View();
		gol.setSize(800, 600);

		gol.setDefaultCloseOperation(3);
		// gol.setUndecorated(true);
		gol.initView(800, 600);

		gol.setVisible(true);

		gol.setResizable(false);

		long lastFrame = System.currentTimeMillis();

		while (true) {

			long thisFrame = System.currentTimeMillis();
			float tslf = (float) ((thisFrame - lastFrame));
			lastFrame = thisFrame;
			// update(tslf);
			update();
			// gol.repaint();

			try {
				Thread.sleep(10);
			} catch (InterruptedException ie) {
				ie.printStackTrace();
			}
		}

	}

    /**
     * Update Game
     */
	private static void update() {
		gol.setInfoLabelText("currentGeneration: "
				+ gol.getModel().getGeneration()
				+ " | UpdateTime: "
				+ gol.getModel().updateTimer.getInitialDelay()
				+ " ms | Memory usage "
				+ (int) ((gol.getModel().getFieldHistorySize() * 100) / gol
						.getModel().getSaves()) + "%");
		gol.update();
		gol.repaint();

	}


    /**
     * Update Game
     * @param tslf Time since last frame
     */
    @Deprecated
	private static void update(float tslf) {

		tslu += tslf;
		if (tslu > PAUSETIME) {
			gol.setInfoLabelText("fixed UpdateTime: " + PAUSETIME
					+ "ms | Latency: [" + (tslu - PAUSETIME) + "] ms | FPS: ["
					+ (int) (1000.0 / tslu) + "]");
			gol.update();
			gol.repaint();
			tslu = 0;
		}
	}

}
