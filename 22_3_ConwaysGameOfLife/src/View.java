import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import javax.print.attribute.standard.JobMessageFromOperator;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SpringLayout;

public class View extends JFrame {

//	private JPanel tPanel = new JPanel(); // BorderLayout North
	private MotionPanel tPanel = new MotionPanel(this);
	private JPanel sPanel = new JPanel(); // BorderLayout Center
	private JPanel iPanel = new JPanel(); // BorderLayout South	
	
	private JPanel bPanel = new JPanel(); // FloatLayout 1 Element inside colPanel
	private JPanel spacerPanel = new JPanel(); // FloatLayout 2 Element inside colPanel
	private JLabel iLabel = new JLabel(""); // FloatLayout 3 Element inside col Panel
	

	private JLabel tLabel = new JLabel("Conway's Game of Life"); // BorderLayout
																	// Center
	
	private JPanel screenPanel;

	GridLayout viewGridLayout;
	GridBagLayout viewGridBagLayout;
	GridBagConstraints gbLConstraints;
	
	SpringLayout viewSpringLayout;
	
	private JGoLField[][] field;
	private boolean[][] bField;
	private Model model;
	private Controller controller;

	// =================
	// setup preferences
	// =================
	
//	private int dimX = 50;
//	private int dimY = 50;
	private int dimX;
	private int dimY;
	private float alivePercentage;
	private int refreshRate;
	private int saves;
	
	private int width;
	private int height;
	
	public List<JButton> buttons = new LinkedList<JButton>();
	
	private final boolean debug = false;
	
	
	
	{
		buttons.add(new JButton("play")); // 0
		buttons.add(new JButton("pause"));// 1
		buttons.add(new JButton("back")); // 2
		buttons.add(new JButton("clear")); //3
		buttons.add(new JButton("random")); //4
		buttons.add(new JButton("exit")); //5
	}
	
	// private Random rand;

	public View() {
		super("");
		this.setUndecorated(true);
		this.dimX = 50;
		this.dimY = 50;
		this.alivePercentage = 0.0f;
		this.refreshRate = 0;
		this.saves = 0;
		this.initAliveChance();
		this.initRefreshRate();
		this.initSaves();
		model = new Model(dimX, dimY, this.alivePercentage, this.refreshRate, this.saves);
		controller = new Controller(model, this);
		
		
	}
	
	public Model getModel(){
		return this.model;
	}

	public void initView(int width, int height) {
		
		this.width = width;
		this.height = height;
		//s = new Screen();
				
		viewGridLayout = new GridLayout(0, 1);
		//this.setLayout(viewGridLayout);
		
		viewSpringLayout = new SpringLayout();
		
		this.screenPanel = new JPanel();
		this.screenPanel.setSize(this.width, this.height-13);;
		this.screenPanel.setLayout(viewSpringLayout);
		//this.screenPanel.setBackground(Color.BLUE);
		this.gbLConstraints = new GridBagConstraints();
		
		
		
//		if(this.model.getPlayStatus()){
//			this.buttons.get(0).setEnabled(false);
//			this.buttons.get(1).setEnabled(true);
//		}else{
//			this.buttons.get(0).setEnabled(true);
//			this.buttons.get(1).setEnabled(false);
//		}
		
		this.buttons.get(0).setEnabled(true);
		this.buttons.get(1).setEnabled(false);
		this.buttons.get(2).setEnabled(false);
		this.buttons.get(3).setEnabled(true);
		this.buttons.get(4).setEnabled(true);
		this.buttons.get(5).setEnabled(true);
		
		
		bField = model.getActualField();
		
		
		this.field = new JGoLField[dimX][dimY];

		
		if(this.debug) {System.out.println(this.width + "x" + this.height);}

		Dimension dStandard = new Dimension(this.width, 50);
		
		
		// Setup TextPanel
		tPanel.setSize(dStandard);
		tPanel.setMinimumSize(dStandard);
		tLabel.setFont(new Font(Font.SERIF, Font.PLAIN, 16));
		tPanel.add(tLabel);
		tPanel.addMouseMotionListener(controller);

		bPanel.setSize(dStandard);
		bPanel.setMinimumSize(dStandard);
		for(JButton b : buttons) { bPanel.add(b); b.addActionListener(controller);}
		
		
		
		iPanel.setSize(dStandard);
		iPanel.setMinimumSize(dStandard);
		iLabel.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 10));
		
		Dimension dScreen = new Dimension(this.width,this.screenPanel.getHeight() - this.tPanel.getHeight() - this.bPanel.getHeight() - this.iPanel.getHeight() - 100);
		
		
		
		
		sPanel.setSize(dScreen);
		sPanel.setMinimumSize(dScreen);
		sPanel.setMaximumSize(dScreen);	
		sPanel.setLayout(new GridLayout(dimX, dimY));

		sPanel.setBackground(Color.WHITE);
		
		for (int i = 0; i < dimY; i++) {
			for (int j = 0; j < dimX; j++) {
				field[i][j] = new JGoLField(i, j, bField[i][j]);
				field[i][j].addMouseListener(controller);
				field[i][j].addMouseMotionListener(controller);
				field[i][j].setWidth((sPanel.getWidth()-dimX) / dimX);
				field[i][j].setHeight((sPanel.getHeight()+3*dimY) / dimY);
				sPanel.add(field[i][j]);
			}
		}
		
		this.iPanel.add(this.iLabel);
		
		screenPanel.add(tPanel);
		screenPanel.add(sPanel);
		screenPanel.add(bPanel);
		screenPanel.add(iPanel);
		
		viewSpringLayout.putConstraint(SpringLayout.WEST, tPanel, 0, SpringLayout.WEST, screenPanel);
		viewSpringLayout.putConstraint(SpringLayout.EAST, tPanel, 0, SpringLayout.EAST, screenPanel);
		viewSpringLayout.putConstraint(SpringLayout.NORTH, tPanel, 0, SpringLayout.NORTH, screenPanel);
		
		viewSpringLayout.putConstraint(SpringLayout.WEST, sPanel, 0, SpringLayout.WEST, screenPanel);
		viewSpringLayout.putConstraint(SpringLayout.EAST, sPanel, 0, SpringLayout.EAST, screenPanel);
		viewSpringLayout.putConstraint(SpringLayout.NORTH, sPanel, 32, SpringLayout.NORTH, screenPanel);
		
		viewSpringLayout.putConstraint(SpringLayout.WEST, bPanel, 0, SpringLayout.WEST, screenPanel);
		viewSpringLayout.putConstraint(SpringLayout.EAST, bPanel, 0, SpringLayout.EAST, screenPanel);
		viewSpringLayout.putConstraint(SpringLayout.NORTH, bPanel,532,SpringLayout.NORTH, screenPanel);
	
		viewSpringLayout.putConstraint(SpringLayout.WEST, iPanel, 0, SpringLayout.WEST, screenPanel);
		viewSpringLayout.putConstraint(SpringLayout.EAST, iPanel, 0, SpringLayout.EAST, screenPanel);
		viewSpringLayout.putConstraint(SpringLayout.NORTH, iPanel,580,SpringLayout.NORTH, screenPanel);
		
		this.add(screenPanel);
		
	}
	
	public void setInfoLabelText(String s){
		this.iLabel.setText(s);
	}

	public void update() {
		
		long debugTimer1 = 0;
		long debugTimer2 = 0;
		
		
		if(this.debug) {debugTimer1 = System.currentTimeMillis();}
		for(int x = 0; x < dimX; x++){
			for(int y = 0; y < dimY; y++){
				this.field[x][y].setAlive((this.model.getActualField())[x][y]);
			}
		}
		if(this.debug) {debugTimer2 = System.currentTimeMillis();}
		if(this.debug) {System.out.println("JGoLField Update takes [" + (debugTimer2 - debugTimer1) + "] ms.");}
		
	}

	private void testChance() {
		int gesamt = 0;
		int counter = 0;
		for (int i = 0; i < 50 * 50; i++) {
			gesamt++;
			if (setState()) {
				counter++;
			}
		}
		System.out.println("Gesamt: " + gesamt + " True: " + counter
				+ " Chance: " + ((counter * 100) / gesamt));
	}

	private boolean setState() {
		Random rand = new Random();
		System.out.println((this.alivePercentage/100.0));
		if (rand.nextFloat() < (this.alivePercentage/100.0)) {
			return true;
		} else
			return false;
	}
	
	private boolean setState(float chance) {
		Random rand = new Random();
		if (rand.nextFloat() < (chance)) {
			return true;
		} else
			return false;
	}
	
	public boolean getPlayStatus(){
		return this.model.getPlayStatus();
	}
	
	public void initAliveChance() {
		
//		String portNumber = (String) JOptionPane.showInputDialog(this,
//		        "Enter the Port number for server creation",
//		        "Server Connection\n", JOptionPane.OK_CANCEL_OPTION, null,
//		        null, "9090");
		
		String input = (String)JOptionPane.showInputDialog(this, "Wieviel Prozent der Zellen sollen lebendig sein? [5..90]", "Initialisiere Alive Chance\n", JOptionPane.QUESTION_MESSAGE,null,null,"10");
        try 
        {
        	float temp = Float.parseFloat(input);
        	if((temp < 5) || (temp > 90)) {
        		JOptionPane.showMessageDialog(null, "Bitte geben sie nur Zahlen zwischen 5 und 90 ein!");
        		this.initAliveChance();
        		
        	}
            this.alivePercentage = Float.parseFloat(input);
            
        } catch (Exception e) 
        {
            JOptionPane.showMessageDialog(null, "Bitte geben sie nur Zahlen zwischen 5 und 90 ein!");
            this.initAliveChance();
        } 
        
        
        
	}
	
	public void initRefreshRate() {
		String input = (String)JOptionPane.showInputDialog(this, "Alle wieviel Millisekunden soll das Feld aktualisiert werden? [1..6000]", "Initialisiere Refresh Rate\n", JOptionPane.QUESTION_MESSAGE,null,null,"250");
        try 
        {
        	int temp = Integer.parseInt(input);
        	if((temp < 1) || (temp > 6000)) {
        		JOptionPane.showMessageDialog(null, "Bitte geben sie nur Zahlen zwischen 1 und 6000 ein!");
        		this.initRefreshRate();        		
        	}
            this.refreshRate = Integer.parseInt(input);
            
        } catch (Exception e) 
        {
            JOptionPane.showMessageDialog(null, "Bitte geben sie nur Zahlen zwischen 1 und 6000 ein!");
            this.initRefreshRate();
        }
	}
	
	public void initSaves() {
		String input = (String)JOptionPane.showInputDialog(this, "Wieviele Generationen sollen im Speicher gehalten werden? [10..1000]", "Initialisiere Speicher\n", JOptionPane.QUESTION_MESSAGE,null,null,"10");
        try 
        {
        	int temp = Integer.parseInt(input);
        	if((temp < 10) || (temp > 1000)) {
        		JOptionPane.showMessageDialog(null, "Bitte geben sie nur Zahlen zwischen 10 und 1000 ein!");
        		this.initSaves();        		
        	}
            this.saves = Integer.parseInt(input);
            
        } catch (Exception e) 
        {
            JOptionPane.showMessageDialog(null, "Bitte geben sie nur Zahlen zwischen 10 und 1000 ein!");
            this.initSaves();
        }
	}
	
	
}
