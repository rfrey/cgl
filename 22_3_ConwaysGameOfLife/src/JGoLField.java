import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JPanel;


public class JGoLField extends JPanel{

	private int row;
	private int col;
	private boolean alive;
	private boolean nextround;
	private int height;
	private int width;
	
	public JGoLField(int r, int c, boolean a){
		super();
		this.row = r;
		this.col = c;
		this.alive = a;
	}
	
	public void setNextRound(boolean nextround){
		this.nextround = nextround;
	}
	
	public void nextRound()
	{
		alive = nextround;
	}
	
	public void setHeight(int height){
		this.height = height;
	}
	
	public void setWidth(int width){
		this.width = width;
	}
	
	public int getRow(){
		return this.row;
	}
	
	public int getCol(){
		return this.col;
	}
	
	public void setAlive(boolean b){
		this.alive = b;
		this.setBackground(this.changeCellColor(b));
	}
	
	public boolean isAlive(){
		return this.alive;
	}
	
	public void update(){
		
	}
	
	public void paintComponent(Graphics g){
		g.setColor(Color.WHITE);
		g.drawRect(0, 0, width, height);
		if(isAlive()) g.setColor(getColor(isAlive()));
		else g.setColor(getColor(isAlive()));
		g.fillRect(0, 0, width, height);
	}
	
	private Color getColor(boolean a) {
		if(a) return Color.DARK_GRAY;
		else return Color.LIGHT_GRAY;
	}

	private Color changeCellColor(boolean state){
		if(state){
			return Color.DARK_GRAY;
		}
		else{
			return Color.GRAY;
		}
	}
}
